//import the needed libraries


import React from "react";

//import bulma components
import "react-bulma-components/dist/react-bulma-components.min.css";
import {
		Navbar,
		Section,
		Heading,
		Columns,
		Card
		} from "react-bulma-components";


class MemberPage extends React.Component {
	render() {
		const sectionStyle = {
			paddingTop: "15px",
			paddingBottom: "15px"
		}
		return (
			//Elements from the pageComponent of index.js
			<React.Fragment>
			<Navbar className="is-black">
				<Navbar.Brand>
					<Navbar.Item>
					<strong>MERNG Tracker</strong>
					</Navbar.Item>
					<Navbar.Burger />
				</Navbar.Brand>

				<Navbar.Menu>
					<Navbar.Container>
					<Navbar.Item>Members</Navbar.Item>
					<Navbar.Item>Teams</Navbar.Item>
					<Navbar.Item>Tasks</Navbar.Item>
					</Navbar.Container>
				</Navbar.Menu>
			</Navbar>

			<Section size="medium" style= {sectionStyle}>
					<Heading>Member</Heading>
					<Columns >
						<Columns.Column className="is-one-quarter">
								<Card>
								<Card.Content>
								<p><strong>Add Member</strong></p>
								<hr />
								<form className="control">
								
								<div style= {sectionStyle}>
								<label>First Name</label>
								<br />
								<input type="text" className="input is-primary" />
								</div>

								<div style= {sectionStyle}>
								<label>Last Name</label>
								<br />
								<input type="text" className="input is-primary" />
								</div>

								<div style= {sectionStyle}>
								<label>Position</label>
								<br />
								<input type="text" className="input is-primary" />
								</div>

								<div className = "field" style= {sectionStyle}>
								<label className="label">Team</label>
								<div className="select">
								<select className="select is-primary">
								
									<option>Select Team</option>
									<option>Select Team 2</option>
									<option>Select Team 3</option>
									<option>Select Team 4</option>
								</select>
								</div>
								</div>

								<div>
								<button className="button is-success">Add</button>
								</div>
								</form>
								</Card.Content>
								</Card>
						</Columns.Column>
						<Columns.Column size={9}>
								<Card>
								<Card.Header>
									<Card.Header.Title>
									Member List
									</Card.Header.Title>
								</Card.Header>

								<Card.Content>
										<div className="table-container">
											<table className="table is-fullwidth is-bordered">
											<thead>
												<th>Member Name</th>
												<th>Position</th>
												<th>Team</th>
												<th>Action</th>
											</thead>
											<tbody>
												<tr>
													<td>Sample</td>
													<td>Sample</td>
													<td>Sample</td>
													<td>Sample</td>
												</tr>
											</tbody>
											</table>
										</div>
								</Card.Content>
								</Card>
						</Columns.Column>

					</Columns>

			</Section>

			</React.Fragment>
			)
	}
}


export default MemberPage
//Follows the name of the file that we use